import { Component, NgZone, OnInit } from '@angular/core';
import { IBeacon } from '@awesome-cordova-plugins/ibeacon/ngx';
import { Platform } from '@ionic/angular';
declare var evothings: any;
import { BLE } from '@awesome-cordova-plugins/ble/ngx';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  beacons: any[]= [];
  constructor(
    private ibeacon: IBeacon,
    private platform: Platform,
    private ngZone: NgZone,
    private ble: BLE,
    private diagnostic: Diagnostic
  ) {}
  ngOnInit(){
    let successCallback = (isAvailable: any) => { console.log('Is available? ' + isAvailable); }
    let errorCallback = (e: any) => console.error(e);
    this.diagnostic.isBluetoothAvailable().then(successCallback, errorCallback);
    this.diagnostic.requestLocationAuthorization('WHEN_IN_USE').then((val: any) => {
      console.log('val', val);
    }).catch((err)=>{
      console.log(err);
    });
    this.diagnostic.getPermissionsAuthorizationStatus([this.diagnostic.permission.ACCESS_FINE_LOCATION,
      this.diagnostic.permission.ACCESS_COARSE_LOCATION]).then((val: any) => {
        console.log('getPermissionsAuthorizationStatus', val);
      }).catch((err: any)=>{
        console.log(err);
      });
  }
  startScan(){
    
    this.platform.ready().then(() => {
      this.ibeacon.requestAlwaysAuthorization();
      this.ibeacon.onDomDelegateReady().then((val) => {
        console.log('ready', val);
      })
      
      // create a new delegate and register it with the native layer
      let delegate = this.ibeacon.Delegate();  
      // Subscribe to some of the delegate's event handlers
      delegate.didRangeBeaconsInRegion().subscribe(
        (data) => {
          //console.log('didRangeBeaconsInRegion: ', JSON.stringify(data))
          const {beacons = []} = data;
          if (beacons.length > 0) {
            this.ngZone.run(() => {
              this.beacons = [...this.beacons, beacons[0].uuid];
            }
            )
            this.beacons.push(beacons[0])
            console.table(beacons);
          }},
        (error) => console.error()
      );
      delegate.didStartMonitoringForRegion().subscribe(
        (data) => console.log('didStartMonitoringForRegion: ', JSON.stringify(data)),
        (error) => console.error()
      );
      delegate.didEnterRegion().subscribe((data) => {
        console.log('didEnterRegion: ', data);
      });
      let beaconRegion = this.ibeacon.BeaconRegion(
        'beacon iBEACON 2',
        '8ec76ea3-6668-48da-9866-75be8bc86f4d',
        0,4,true
      );
      
        this.ibeacon.enableDebugLogs().then((val) => {
          console.log(JSON.stringify(val));
        })
        this.ibeacon.startRangingBeaconsInRegion(beaconRegion).then((val: any) => {
           console.log('aaaa IBEACON', JSON.stringify(val));
        })
      this.ibeacon.startMonitoringForRegion(beaconRegion).then(
        () => console.log('Native layer received the request to monitoring'),
       (error) =>
          console.error('Native layer failed to begin monitoring: ', error)
      );
    })
    
  }
  startScanEddy(){
    this.platform.ready().then(()=> {
      evothings.eddystone.startScan(
        (beacon: any) => {
          console.log('BEACONNN', beacon);
          // do something with beacon data
      },
      (error: any) => {
        console.log('ERROR', error);
          // do something with error
      }
      )
    })
  }
  startScanBLE(){
    this.ble.scan([], 60).subscribe((data) => {
      console.log(JSON.stringify(data));
    });
  }
}
